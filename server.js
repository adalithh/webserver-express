
const express = require('express')
const app = express()

const hbs = require('hbs');
require('./hbs/helpers')


const port = process.env.PORT || 3000

app.use( express.static( __dirname + '/public'))

hbs.registerPartials( __dirname + '/views/parciales')
//Express HBS engine
app.set('view engine', 'hbs');






app.get('/', (req, res) => { 
   
   res.render('home', {
      nombre: 'Adalith',
      anio : new Date().getFullYear()
   });
})

app.get('/abaut', (req, res) => { 
   
   res.render('abaut', {
      anio : new Date().getFullYear()
   });
})


app.listen(port, () => {
   console.log(`Escuchando peticiones en el puerto ${ port }`);
})


